<?php

class Blog extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('post');
  }

  public function index() 
  {
    echo "Hello world";
  }

  public function comments() 
  {
    echo "Look at this!";
  }

  private function _utility()
  {
    echo "This cannot access from public";
  }
}